const initialDefaultState = {
    imagePickerTypeStatus: false

}

export default (state = initialDefaultState, action) => {
    console.log('action : ',action);
    switch(action.type){
        case 'CLOSE_MODAL':
            return {...state, imagePickerTypeStatus: false }
        case 'OPEN_MODAL':
            return {...state, imagePickerTypeStatus: true }
        default:
            return {...state}
    }
}