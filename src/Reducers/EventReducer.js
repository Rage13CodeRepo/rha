const initialDefaultState = {
    driveId : '',
    driveName: '',
    driveType: '',
    driveStatus: '',
    meetingLocation: '',
    driveLocation: '',
    time: '',
    date: '',
    peopleServed: '',
    foodProviders: '',
    chapter: {},
    publish: false
}

export default (state = initialDefaultState, action) => {
    switch(action) {
        default:
            return { ...state }
    }
}