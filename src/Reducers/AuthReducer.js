const initialDefaultState = {
    email_login: '',
    password_login: '',
    email: '',
    password: '',
    confirmPassword: '',
    name: '',
    contactNumber: '',
    gender: -1,
    userType: 0,
    userLocation: {
        latitude: 20.5937,
        longitude: 78.9629
    },
    loading: false,
    // user: {
    //     user: {
    //         uid: "7cYMRZnVESfrSxwDoYAw4w0tkKf2"
    //     }
    // },
    user: null,
    error: null,
    chapters: null,
    selectedChapter: null,
    selectedChapterMemberCount: 0,
    locationPermission: false,
    // usersAdditionalInfo: {
    //     "affiliatedChapter" : "chp2",
    //     "contactNumber" : "9963336996",
    //     "gender" : 1,
    //     "location" : {
    //       "lat" : "",
    //       "lng" : ""
    //     },
    //     "name" : "test123",
    //     "profilePicture" : "",
    //     "userType" : 0
    // }
    usersAdditionalInfo: '',
    isConnected: true
};

export default (state = initialDefaultState, action) => {
    console.log(action);
    switch(action.type) {
        case 'EMAIL_CHANGED':
            return { ...state, email: action.payload }
        case 'PASSWORD_CHANGED':
            return { ...state, password: action.payload }
        case 'CONFIRM_PASSWORD_CHANGED':
            return { ...state, confirmPassword: action.payload }
        case 'NAME_CHANGED':
            return { ...state, name: action.payload }
        case 'CONTACT_NUMBER_CHANGED':
            return { ...state, contactNumber: action.payload }
        case 'GENDER_CHANGED':
            return { ...state, gender: action.payload }
        case 'USER_TYPE_CHANGED':
            return { ...state, userType: action.payload }
        case 'EMAIL_LOGIN_CHANGED':
            return { ...state, email_login: action.payload }
        case 'PASSWORD_LOGIN_CHANGED':
            return { ...state, password_login: action.payload }
        case 'LOADING':
            return { ...state, loading: true }
        case 'LOADED':
            return { ...state, loading: false }
        case 'SET_DEFAULT_USER':
            return { ...state, user: null }
        case 'LOGIN_SUCCESS':
            return { ...state, loading: false, user: action.payload, email_login: '', password_login: '' }
        case 'GET_USERS_ADDITIONAL_INFO':
            return { ...state, usersAdditionalInfo: action.payload }
        case 'LOGIN_FAILURE':
            return { ...state, loading: false, error: action.payload }
        case 'SIGNUP_SUCCESS':
            return { ...state, loading: false, email: '', password: '', confirmPassword: '', error: null, user: action.payload }
        case 'SIGNUP_FAILURE':
            return { ...state, loading: false }
        case 'ADDITIONAL_INFO_SUCCESS':
            return { ...state, loading: false, name: '', contactNumber: '', gender: -1, userType: -1, usersAdditionalInfo: action.payload }
        case 'SET_USER_LOCATION':
            return { ...state, userLocation: action.payload }
        case 'GET_ALL_CHAPTERS':
            return { ...state, chapters: action.payload }
        case 'SET_CHAPTER':
            return { ...state, selectedChapter: action.payload,  }
        case 'SET_USERSADDITIONALINFO_CHAPTER':
            return { ...state, usersAdditionalInfo: { ...state.usersAdditionalInfo, affiliatedChapter: action.payload }}
        case 'SELECTED_CHAPTER_MEMBER_COUNT':
            return { ...state, selectedChapterMemberCount: action.payload }
        case 'PERMISSION_APPROVED':
            return { ...state, locationPermission:true }
        case 'CONNECTION_UNAVAILABLE':
            return { ...state, isConnected: false }
        case 'CONNECTION_AVAILABLE':
            return { ...state, isConnected: true }
        case 'ERROR':
            return { ...state, error: action.payload }
        case 'CLEAR_ERROR':
            return { ...state, error: null }
        default:
            return state;
    }
}