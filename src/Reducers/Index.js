import { combineReducers } from "redux";
import AuthReducer from "./AuthReducer";
import ChatReducer from "./ChatReducer";
import EventReducer from "./EventReducer";
import SettingReducer from "./SettingReducer";

const reducer = combineReducers({
    auth: AuthReducer,
    chat: ChatReducer,
    event: EventReducer,
    setting: SettingReducer
});

export default reducer;
