const initialDefaultState = {
    message: '',
    messages: [
        
    ],
    loading: false,
    settings: {
        allowPushNotifications: false
    }
}

export default (state = initialDefaultState, action) => {
    console.log(action);
    switch(action.type) {
        case 'NEW_MESSAGE':
            return { ...state, message: action.payload }
        case 'SEND_MESSAGE':
            return { ...state, messages: action.payload.concat(state.messages) }
        case 'LOAD_MESSAGES':
            return { ...state, messages: action.payload }
        case 'CLEAR_MESSAGES':
            return { ...state, messages: []}
        default:
            return { ...state }
    }
}