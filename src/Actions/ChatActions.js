import database, { firebase } from "../Components/Common/Firebase";
import { Actions } from "react-native-router-flux";

export const chatMessageChanged = (textInput) => {
    return {
        type: 'NEW_MESSAGE',
        payload: textInput
    };
};

export const messageSend = (textMessage) => {
    return(dispatch) => {
        let msgTimeStmp = Math.floor(new Date().getTime() / 1000);
        dispatch({type:'SEND_MESSAGE', payload: textMessage});
        database.ref('chats/'+ textMessage[0].user.chapter + '/' + msgTimeStmp + ' - ' + textMessage[0]._id ).set({
            _id: textMessage[0]._id,
            createdAt: textMessage[0].createdAt.toString(),
            text: textMessage[0].text,
            user: {
                _id: textMessage[0].user._id,
                name: textMessage[0].user.name,
                avatar: textMessage[0].user.avatar,
                chapter: textMessage[0].user.chapter
            }
        }) 
        .then(() => {

        })
    }
}

export const loadMessages = (userInfo) => {
    return(dispatch) => {
        database.ref('chats/' + userInfo.affiliatedChapter ).on('value', (snapshot) => {
            const messagesJSON = snapshot.val();
            console.log(messagesJSON);
            if (messagesJSON) {
                const messages = Object.values(messagesJSON).reverse();
                dispatch({type:'LOAD_MESSAGES', payload: messages});
            }
        })
    }
}

export const changePushNotificationSettings = (setting) => {
    return(disptach) => {
        disptach({type:'CHANGE_CHAT_PUSH_NOTIFICATIONS', payload: setting });
    }
}

export const clearMessages = () => {
    return(disptach) => {
        disptach({type:'CLEAR_MESSAGES'});
    }
}