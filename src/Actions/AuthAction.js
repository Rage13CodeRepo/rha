import database, { firebase } from "../Components/Common/Firebase";
import { Actions, ActionConst } from "react-native-router-flux";
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import { BackHandler } from "react-native";



export const loginEmailChanged = (textInput) => {
    return {
        type: 'EMAIL_LOGIN_CHANGED',
        payload: textInput
    };
};

export const loginPasswordChanged = (textInput) => {
    return {
        type: 'PASSWORD_LOGIN_CHANGED',
        payload: textInput
    };
};

export const emailChanged = (textInput) => {
    return {
        type: 'EMAIL_CHANGED',
        payload: textInput
    };
};

export const passwordChanged = (textInput) => {
    return {
        type: 'PASSWORD_CHANGED',
        payload: textInput
    };
};

export const confirmPasswordChanged = (textInput) => {
    return {
        type: 'CONFIRM_PASSWORD_CHANGED',
        payload: textInput
    };
};

export const nameChanged = (textInput) => {
    return {
        type: 'NAME_CHANGED',
        payload: textInput
    };
};

export const contactNumberChanged = (textInput) => {
    return {
        type: 'CONTACT_NUMBER_CHANGED',
        payload:textInput
    };
};

export const genderChanged = (selectedIndex) => {
    return {
        type: 'GENDER_CHANGED',
        payload: selectedIndex
    };
};

export const userTypeChanged = (selectedIndex) => {
    return {
        type: 'USER_TYPE_CHANGED',
        payload: selectedIndex
    };
};

export const loginUser = ({email, password}) => {

    return(dispatch) => {
        dispatch({type:'CLEAR_ERROR'});
        if(email == "" || password == "") {
            dispatch({type:'ERROR', payload: 'It seems like you have left one or more input fields empty'});
        }
        else {
            dispatch({type:'LOADING'});
            firebase.auth().signInWithEmailAndPassword(email, password)
            .then(user => {
                console.log(user)
                dispatch({ type: 'LOGIN_SUCCESS', payload: user });
                database.ref('users/'+ user.user.uid).once("value")
                .then(snapshot => {
                    dispatch({type: 'GET_USERS_ADDITIONAL_INFO', payload: snapshot.val()})
                    Actions.main({type:'replace'});
                })
            })
            .catch(error => {
                dispatch({ type: 'LOGIN_FAILURE'});
                dispatch({type:'ERROR', payload: error.message})
                console.log(error);
            });
        }
    }
}

export const signupUser = ({email, password, confirmPassword}) => {
    return (dispatch) => {
        dispatch({ type: 'CLEAR_ERROR' });
        if(email == "" || password == "" || confirmPassword == "") {
            dispatch({type:'ERROR', payload: 'It seems like you have left one or more input fields empty'});
        }
        else if (password == "" || confirmPassword == "" || password === confirmPassword) {
            dispatch({ type: 'LOADING' });
            firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(user => {
                dispatch({ type: 'SIGNUP_SUCCESS', payload: user });
                Actions.additionalinfo({type:'replace'});
            })
            .catch(error => {
                dispatch({type: 'SIGNUP_FAILURE'});
                dispatch({type:'ERROR', payload: error.message})
                console.log(error);
            });
        }
        else {
            dispatch({type:'ERROR', payload: 'Incorrect passwords'});
            dispatch({type: 'SIGNUP_FAILURE'});
        }
    };
};

export const userVerification = (user) => {
    return(dispatch) => {
        dispatch({type:'LOADING'});
        if(user) {
            database.ref('users/'+ user.user.uid).once('value')
            .then((snapshot) => {
                if(snapshot.val() !== null) {
                    console.log(snapshot.val());
                    Actions.main({type:'replace'});
                } 
            });
        }
        dispatch({type:'LOADED'});
    }
}

export const redirectToSignup = () => {
    return (dispatch) => {
        Actions.signup({type:'replace'});
    }
}

export const addAdditionalInfo = ({uid, name, contactNumber, gender, userType}) => {
    return (dispatch) => {
        dispatch({type:"CLEAR_ERROR"});
        console.log(contactNumber.toString().length);
        console.log(uid, name, contactNumber, gender, userType);       
        if (uid === '' || name === '' || contactNumber === '' || gender === -1 || userType < -1) {
            dispatch({type:'ERROR', payload: 'It seems like you have left one or more input fields empty'});
        }
        else if(contactNumber.toString().length !== 10) {
            dispatch({type:'ERROR', payload: 'It seems like you have entered and incorrect contact number'});
        }
        else { 
            //dispatch({type: 'LOADING'});
            database.ref('users/'+ uid).set({
                name: name,
                contactNumber: contactNumber,
                gender: gender,
                userType: userType,
                profilePicture: '',
                location: {
                    lat: '',
                    lng: ''
                },
                affiliatedChapter: 'null'
            })
            .then(() => {
                database.ref('users/'+ uid).once('value').
                then(snapshot => {
                    dispatch({type: 'ADDITIONAL_INFO_SUCCESS', payload: snapshot.val()});
                    Actions.joinchapter({type:'replace'});
                });
            })
            .catch(e => console.log(e));
        }
    }
}

export const setUserLocation = (location) => {
    return {
        type: 'SET_USER_LOCATION',
        payload: location
    }
}

export const getAllChapters = () => {
    return(dispatch) => {
        database.ref('/chapters').once('value')
        .then(snapshot => {
            const chapters = snapshot.val();
            dispatch({ type: 'GET_ALL_CHAPTERS', payload: chapters });
        })
        .catch(e => console.log(e));
        dispatch({type:'LOADED'})
    }
}

export const setChapter = (chapter) => {
    return(dispatch) => {
        dispatch({ type: 'SET_CHAPTER', payload: chapter });
        Actions.chapterdetails();
    }
}

export const joinChapter = (chapter, user, date) => {
    return(dispatch) => {
        console.log(chapter);
        database.ref('members/'+ chapter.chapterId + '/'+ user.user.uid).set({
            joinDate: date,
            status: 'Active'
        })
        .then(snapshot => {
            database.ref('users/'+ user.user.uid + '/affiliatedChapter').set(chapter.chapterId)           
            .then(snapshot => {
                dispatch({type:'SET_USERSADDITIONALINFO_CHAPTER', payload: chapter.chapterId});
                Actions.main({type:'replace'});
            })
        })
        .catch(e => console.log(e));
    }
}

export const membersCount = (chapter) => {
    return(dispatch) => {
        let count = 0;
        database.ref('members/'+ chapter.chapterId).once('value')
        .then(snapshot => {
            const membersList= snapshot.val();
            if (membersList !== null)
            {
                Object.keys(membersList).map(key => {
                    count++;
                });
            }
            dispatch({type: 'SELECTED_CHAPTER_MEMBER_COUNT', payload: parseInt(count)});
        })
        .catch(e => console.log(e));
        
    }
}

export const requestLocationPermission = () => {
    return(dispatch) => {
        LocationServicesDialogBox.checkLocationServicesIsEnabled({
            message: "<h2 style='color: #0af13e'>Use Location ?</h2>The RHA app wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/><a href='#'>Learn more</a>",
            ok: "YES",
            cancel: "NO",
            enableHighAccuracy: true, 
            showDialog: true,
            openLocationServices: true, 
            preventOutSideTouch: false, 
            preventBackClick: true, 
            providerListener: false 
        }).then(function(success) {
            dispatch({type:'PERMISSION_APPROVED'})
        }).catch((error) => {
            dispatch({type:'PERMISSION_DENIED'})
        });
        
        BackHandler.addEventListener('hardwareBackPress', () => { 
            LocationServicesDialogBox.forceCloseDialog();
        });
    }
}

export const connectionNotFound = () => {
    return(dispatch) => {
        dispatch({type:"CONNECTION_UNAVAILABLE"});
    }
}

export const clearError = () => {
    return(dispatch) => {
        dispatch({type:"CLEAR_ERROR"})
    }
}