import database, { firebase } from "../Components/Common/Firebase";
import { Actions, ActionConst } from "react-native-router-flux";
import ImagePicker from "react-native-image-crop-picker";
import RNFetchBlob from "rn-fetch-blob";

export const closeImagePickerTypeModal = () => {
    return(dispatch) => {
        dispatch({type: 'CLOSE_MODAL'}); 
    }
}

export const openImagePickerTypeModal = () => {
    return(dispatch) => {
        dispatch({type: 'OPEN_MODAL'});
    }
}

export const openCamera = (user) => {
    return(dispatch) => {
        dispatch({type: 'CLOSE_MODAL'}); 
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
            cropperCircleOverlay: true
        }).then(image => {
            console.log(image);
            RNFetchBlob.fs.readFile(image.path,'base64')
            .then((datat) => {
                console.log(user);
                console.log(data);
                database.ref("users/" + user.user.uid + "/profilePicture").set(data);
            })
        })
        console.log("Open Image");
    }
}

export const openFiles = (user) => {
    return(dispatch) => {
        dispatch({type: 'CLOSE_MODAL'}); 
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true,
            cropperCircleOverlay: true
        }).then(image => {
            console.log(image);
            RNFetchBlob.fs.readFile(image.path,'base64')
            .then((data) => {
                console.log(user);
                console.log(data);
                database.ref("users/" + user.user.uid + "/profilePicture").set(data);
            })
        })
        console.log("Open Image");
    }
}