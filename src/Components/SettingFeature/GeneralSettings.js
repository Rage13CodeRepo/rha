import React, { Component } from "react";
import { View, Text, StyleSheet, Dimensions, Image } from "react-native";
import { Card, Avatar, ListItem, Button } from "react-native-elements";
import ImagePicker from 'react-native-image-crop-picker';
import Modal from "react-native-modal";
import { connect } from "react-redux";
import { closeImagePickerTypeModal, openImagePickerTypeModal, openCamera, openFiles } from "../../Actions/SettingAction";


class GeneralSetting extends Component {
   constructor() {
       super();
   }

   componentWillReceiveProps() {
       console.log(this.props.imagePickerTypeStatus);
   }

    render() {
        return(
            <View style = {styles.settingsContainer}>
            <MainHeader title = "" leftButton = "arrow-back" rightButton = "edit" leftAction = "back" />
            
                <View style={styles.header}></View>
                <Avatar size="xlarge" rounded source = {{uri: 'data:image/png;base64,' + this.props.usersAdditionalInfo.profilePicture}} containerStyle={styles.avatar} onPress={this.props.openImagePickerTypeModal}/>
                <Modal isVisible = {this.props.imagePickerTypeStatus}
                    onBackdropPress = {this.props.closeImagePickerTypeModal}
                    onBackButtonPress = {this.props.closeImagePickerTypeModal}
                    animationInTiming = {500}
                    animationOutTiming = {500}
                    backdropTransitionInTiming = {500}
                    backdropTransitionOutTiming = {500}
                    hideModalContentWhileAnimating = {true}
                    useNativeDriver = {true}
                >
                    <View style = {{flex:1}}>
                        <Card containerStyle = {{marginTop:Dimensions.get('window').height/2-100,backgroundColor: 'white', borderRadius:15}}>
                            <ListItem 
                                style = {{marginTop:10}}
                                containerStyle = {{backgroundColor: 'white'}}
                                title = "Take Picture"
                                titleStyle = {{color:'#696969'}}
                                hideChevron
                                bottomDivider
                                onPress = {() => this.props.openCamera(this.props.user)}
                            />
                            <ListItem
                                style = {{marginTop:10}}
                                containerStyle = {{backgroundColor: 'white'}}
                                titleStyle = {{color:'#696969'}}
                                title = "Find on my device"
                                hideChevron
                                onPress = {() => this.props.openFiles(this.props.user)}
                            />
                        </Card>
                    </View>
                </Modal>
                <View style={styles.body}>
                    <View style={styles.bodyContent}>
                        <Text style={styles.name}>Saish Karapurkar</Text>
                    </View>
                    <Card containerStyle = {{marginTop:35,backgroundColor: 'rgba(255,255,255,.2)'}}>
                        <ListItem 
                            leftIcon = {{name: 'email'}}
                            style = {{marginTop:10}}
                            containerStyle = {{backgroundColor: 'rgba(255,255,255,0)'}}
                            title = "srage13@gmail.com"
                            titleStyle = {{color:'#696969'}}
                            hideChevron
                            bottomDivider
                        />
                        <ListItem 
                            leftIcon = {{name: 'local-phone'}}
                            style = {{marginTop:10}}
                            containerStyle = {{backgroundColor: 'rgba(255,255,255,0)'}}
                            titleStyle = {{color:'#696969'}}
                            title = "9850670654"
                            hideChevron
                        />
                    </Card>
                </View>
            
        </View>
        ); 
    }
}

const styles = StyleSheet.create({
    settingsContainer: {
        flex:1,
    },
    container: {
        marginTop:25,
        padding: 0,
        backgroundColor: 'rgba(239,247,253, 0)',
        marginBottom:100,
    },
    header:{
        backgroundColor: "#378555",
        height:100,
        marginTop:-5
      },
      avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 5,
        borderColor: "#EEEEEE",
        marginBottom:10,
        alignSelf:'center',
        position: 'absolute',
        marginTop:90
      },
      name:{
        fontSize:22,
        color:"#FFFFFF",
        fontWeight:'600',
        alignSelf: 'center'
      },
      body:{
        marginTop:40,
      },
      bodyContent: {
        flex: 1,
        alignItems: 'center',
        padding:30,
      },
      name:{
        fontSize:28,
        color: "#696969",
        fontWeight: "600"
      },
      info:{
        fontSize:16,
        color: "#00BFFF",
        marginTop:10
      },
      description:{
        fontSize:16,
        color: "#696969",
        marginTop:10,
        textAlign: 'center'
      },
      buttonContainer: {
        marginTop:10,
        height:45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:20,
        width:250,
        borderRadius:30,
        backgroundColor: "#00BFFF",
      },
})

const mapDispatchToProps = {
    closeImagePickerTypeModal,
    openImagePickerTypeModal,
    openCamera,
    openFiles
}

const mapStateToProps = state => {
    return {
        imagePickerTypeStatus: state.setting.imagePickerTypeStatus,
        user: state.auth.user,
        usersAdditionalInfo: state.auth.usersAdditionalInfo
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GeneralSetting);