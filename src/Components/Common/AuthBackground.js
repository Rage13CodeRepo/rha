import React from "react";
import { Dimensions, Image } from "react-native";
import { CachedImage } from "react-native-cached-image"

export default AuthBackground = () => {
    return(
        <CachedImage style = {{ width: Dimensions.get('screen').width, height: Dimensions.get('screen').height, position:"absolute"}} resizeMode='cover' source = { require("../../../assets/images/RHA.jpg")}/>   
    )
}