import React, { Component } from "react";
import { StyleSheet, View, Text, NetInfo } from "react-native";
import { userVerification, redirectToSignup, connectionNotFound } from "../../Actions/AuthAction";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import { Card } from "react-native-elements";
import AuthBackground from "./AuthBackground";


class Splash extends Component {
    constructor(props) {
        super(props);
        this.checkIfUserExists = this.checkIfUserExists.bind(this);
        this.checkConnectionStatus = this.checkConnectionStatus.bind(this);
    }

    componentWillMount() {
        this.checkConnectionStatus();
        NetInfo.isConnected.addEventListener(
            'connectionChange',
            this.checkConnectionStatus
        )
    }

    componentDidMount() {
        console.log("Component Did Mount");
        
        //this.checkIfUserExists();
    }

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this.checkConnectionStatus
        );
    }

    checkConnectionStatus() {
        NetInfo.getConnectionInfo().then((connectionInfo) => {
            if(connectionInfo.type == "wifi" || connectionInfo.type == "cellular") {
                if(this.props.user) {
                    console.log("signup");
                    Actions.main({type:"replace"})
                }
                else {
                    console.log("main");
                    Actions.signup({type:"replace"});
                }
            }
            else if(connectionInfo.type == "none") {
                this.props.connectionNotFound();
            }
            else {
                console.log("Connection was not established else");
                this.props.connectionNotFound();
            }
        });
    }

    handleConnectivityChange(connectionInfo) {
        this.checkConnectionStatus();
    }

    checkIfUserExists() {
        console.log('function called');
        const { user } = this.props;
        if(user) {
            this.props.userVerification(user);
        }
        else {
            console.log('Redirect to signup page');
            this.props.redirectToSignup();
        }
    }

    render() {
        return(
            <View style = {style.splashContainer}>
            <AuthBackground/>
                {
                    !this.props.isConnected ? <Card containerStyle = {style.cardContainer}><Text>This app requires that you connect to the internet, Please turn on network connection and try again...</Text></Card>:<View></View> 
                }
            </View>
        )
    }
}

const style = StyleSheet.create({
    splashContainer: {
        flex:1,
        justifyContent:'center'
    },
    cardContainer: {
        marginTop:25,
        padding: 20,
        backgroundColor: 'rgba(239,247,253, 0.8)',
        marginBottom:100,
    },

});

const mapDispatchToProps = {
    userVerification,
    redirectToSignup,
    connectionNotFound
}

const mapStateToProps = state => {
    return{
        user: state.auth.user,
        isConnected: state.auth.isConnected
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Splash);