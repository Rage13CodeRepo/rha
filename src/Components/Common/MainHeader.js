import React from "react";
import { Header } from "react-native-elements";
import { Actions } from "react-native-router-flux";

export default MainHeader = (props) => {

    onLeftIconPress = () => {
        if(props.leftAction) {
            const action = props.leftAction;
            console.log(props.leftAction)
            if(action === "back")
                Actions.pop();
            //else
        }
    }

    onRightIconPress = () => {
        if(props.rightAction) {
            const action = props.rightAction;
            console.log(action);
            switch(action) {
                case 'chatsettings':
                    Actions.chatsettings();
            }
        }
    }

    return(
        <Header
            containerStyle = {{ backgroundColor:'#378555',maxHeight:70, flex:1}}
            centerComponent={{ text: props.title ? props.title : '', style: { color: '#fff', fontSize:20, marginBottom:25 } }}
            rightComponent = {{ icon: props.rightButton ? props.rightButton === "none" ? '' : props.rightButton : 'settings', color:'#fff', underlayColor: 'transparent', onPress: onRightIconPress }} 
            rightContainerStyle = {{marginBottom:20}}
            leftComponent={{ icon: props.leftButton ? props.leftButton === "none" ? '' : props.leftButton : 'menu', color: '#fff', underlayColor: 'transparent', onPress: onLeftIconPress }}
            leftContainerStyle = {{marginBottom:20}}
        />
    );
}