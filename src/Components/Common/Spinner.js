import React from "react";
import { View, StyleSheet } from "react-native";
import LottieView from 'lottie-react-native';
import AuthBackground from "./AuthBackground";

const Spinner = () => {
    return(
        <View style = {styles.spinnerContainer}>
            <AuthBackground/>
            <LottieView
                source={require('../../../assets/data/RHALoadingAnimation.json')}
                autoPlay
                loop
            /> 
        </View>
    );
}

const styles = StyleSheet.create({
    spinnerContainer: {
        flex:1,
        justifyContent: 'center',
        alignContent: 'center',
    }
});

export default Spinner;