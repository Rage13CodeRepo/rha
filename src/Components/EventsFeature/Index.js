import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import MainHeader from "../Common/MainHeader";
import { Actions, Scene, ActionConst } from "react-native-router-flux";
import { Upcoming } from "../EventsFeature/Upcoming";
import { Ongoing } from "../EventsFeature/Ongoing";
import { Completed } from "../EventsFeature/Completed";

class Events extends Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        Actions.create(
            
        );
    }

    render() {
        return(
            <View style = {styles.container}>
            <MainHeader title = "Events" leftButton = "menu" rightButton = "settings" />
                <Text>This is the Events Component</Text>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1
    }
});

export default Events;