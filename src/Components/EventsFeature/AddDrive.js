import React, { Component } from "react";
import { StyleSheet, View, Picker } from "react-native";
import { Text, Card } from "react-native-elements";
import MainHeader from "../Common/MainHeader";
import { Dropdown } from 'react-native-material-dropdown';

class AddDrive extends Component {

    constructor() {
        super();
    }
    
    render() {
        return(
            <View style = {styles.viewContainer}> 
                <MainHeader title = "Add Drive" leftButton = "arrow-back" leftAction = "back" rightButton = "none" />
                <Card>
                    <Dropdown label = "Drive Type"
                        animationDuration = {100}
                        containerStyle = {{}}
                        rippleOpacity = {0.07}
                        shadeOpacity = {0}
                        rippleCentered = {true}
                        itemTextStyle = {{marginBottom: 5,}}
                        data = {[
                            { value: '1', label: 'Ad-Hoc' },
                            { value: '2', label: 'Regular' },
                            { value: '3', label: 'Special' },
                            { value: '4', label: 'Collabrative' },
                          
                    ]}/>
                    

                </Card>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewContainer: {
        flex:1
    }
})

export default AddDrive;