import React, { Component } from "react";
import { View, StyleSheet, FlatList, ScrollView } from "react-native";
import DriveInfo from "./DriveInfo";
import AddDriveButton from "./AddDriveButton";

export default class Upcoming extends Component {
    render() {
        return(
            <View style = {styles.container}>
                <FlatList
                    data = {"123123123"}
                    renderItem = { ({item}) => (
                        <DriveInfo/>
                    )}
                />
                <AddDriveButton/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1
    }
});