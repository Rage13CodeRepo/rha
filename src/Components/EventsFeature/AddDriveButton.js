import React from "react";
import { Icon } from "react-native-elements";
import { FloatingAction } from "react-native-floating-action";
import { Actions } from "react-native-router-flux";

export default AddDriveButton = () => {
    return(
        <FloatingAction floatingIcon = {<Icon name='add' type='MaterialIcons' color='white' size = {35} backgroundColor = "#378555"/>}
                        overlayColor = "none"
                        style = {{backgroundColor:'#378555'}}
                        dismissKeyboardOnPress
                        onPressMain = {Actions.adddrive}
                        showBackground = {false}
                        
        />
  
    );
}