import React, { Component } from "react";
import { View, StyleSheet, FlatList } from "react-native";
import DriveInfo from "./DriveInfo";

export default class Completed extends Component {
    render() {
        return(
            <View style = {styles.container}>
                <FlatList
                    data = {"123"}
                    renderItem = { ({item}) => (
                        <DriveInfo/>
                    )}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1
    }
});