import React, { Component } from "react";
import AuthBackground from "../Common/AuthBackground";
import { StyleSheet, View } from "react-native";
import { Card, Input, Button, Text } from "react-native-elements";
import { emailChanged, passwordChanged, confirmPasswordChanged, signupUser, clearError } from "../../Actions/AuthAction";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import Spinner from "../Common/Spinner";
import Toast, { DURATION } from 'react-native-easy-toast-fixed';


class Signup extends Component {
    constructor(props) {
        super(props);
        this.onSignupPress = this.onSignupPress.bind(this);
    }

    componentWillMount() {
        this.props.clearError();
    }

    componentWillReceiveProps(newProps) {
        if(newProps.error != this.props.error) {
            if(newProps.error !== null)
                this.refs.toast.show(newProps.error, DURATION.FOREVER);
            else
                this.refs.toast.show();

        }      
    }

    onSignupPress() {
        const { email, password, confirmPassword } =this.props;
        this.props.signupUser({email, password, confirmPassword});
    }

   

    render() {
        return(
            <View style = {styles.loginContainer}>
            <AuthBackground/> 
            {
                this.props.loading ? <Spinner/> : this.props.user ? <View></View> :
                <View>
                    
                    <Card containerStyle = {styles.cardContainer}>
                        <Input
                            keyboardType = "email-address"
                            placeholder = "Enter your email here"
                            leftIcon = {{ type: 'MaterialIcons', name: 'email' }}
                            inputContainerStyle = {styles.inputContainerStyle}
                            containerStyle = {styles.containerStyle}
                            inputStyle = {{height:60}}
                            onChangeText = {(textInput) => this.props.emailChanged(textInput)}
                            value = {this.props.email}
                        />
                        <Input
                            secureTextEntry
                            placeholder = "Password"
                            leftIcon = {{ type: 'MaterialIcons', name: 'lock-outline' }}
                            inputContainerStyle = {styles.inputContainerStyle}
                            containerStyle = {styles.containerStyle}
                            inputStyle = {{height:60}}
                            onChangeText = {(textInput) => this.props.passwordChanged(textInput)}
                            value = {this.props.password}
                        />

                        <Input
                            secureTextEntry
                            placeholder = "Re-Enter Password"
                            leftIcon = {{ type: 'MaterialIcons', name: 'lock-open' }}
                            inputContainerStyle = {styles.inputContainerStyle}
                            containerStyle = {styles.containerStyle}
                            inputStyle = {{height:60}}
                            onChangeText = {(textInput) => this.props.confirmPasswordChanged(textInput)}
                            value = {this.props.confirmPassword}
                        />

                        <Button
                            title='Signup'
                            buttonStyle = {styles.buttonStyle}
                            onPress = {this.onSignupPress}
                        />
                        
                    </Card>
                    <Toast
                        ref="toast"
                        style={{backgroundColor:'#D54F44', borderRadius:5}}
                        position='bottom'
                        positionValue={300}
                        opacity={0.8}
                        textStyle={{color:'white'}}
                    />
                    <Text style = {{textAlign:"center", color:'#ffffff', textDecorationLine: 'underline', textDecorationColor:'#ffffff', fontSize:15, fontWeight: 'bold' }} onPress = {() => Actions.login({type:'replace'})}>Already have an account ? Login !</Text>
                </View>
            }
            </View>
        );
    }
}


const styles = StyleSheet.create({
    loginContainer:{
        flex:1,
        justifyContent: 'center',
    },
    inputContainerStyle: {
        marginTop: 15,
        height: 60,
        borderColor: '#252525',
        borderWidth: 1,
        borderRadius: 25,
    },
    containerStyle: {
        paddingBottom: 10,
        marginLeft: 15,
    },
    cardContainer: {
        marginTop:25,
        padding: 20,
        backgroundColor: 'rgba(239,247,253, 0.8)',
        marginBottom:100,
    },
    buttonStyle: {
        marginTop: 15,
        marginBottom:15
    }
});

const mapDispatchToProps = {
    emailChanged,
    passwordChanged,
    confirmPasswordChanged,
    signupUser,
    clearError
}

const mapStateToProps = state => {
    return {
        email: state.auth.email,
        password: state.auth.password,
        confirmPassword: state.auth.confirmPassword,
        loading: state.auth.loading,
        user: state.auth.user,
        error: state.auth.error
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);