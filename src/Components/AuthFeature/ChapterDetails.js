import React, { Component } from "react";
import AuthBackground from "../Common/AuthBackground";
import { Header, Card, Button, Divider, ListItem } from "react-native-elements";
import { View, StyleSheet, Dimensions } from "react-native";
import { connect } from "react-redux";
import { joinChapter, membersCount } from "../../Actions/AuthAction";
import { Actions } from "react-native-router-flux";
import { Spinner } from "../Common/Spinner";

getDate = () => {
    let today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth()+1;
    const yyyy = today.getFullYear();

    if (dd<10)
        dd = '0'+dd;
    if (mm<10)
        mm = '0'+mm;

    return today = dd+ '/' + mm + '/' + yyyy;

}

class ChapterDetails extends Component {

    constructor() {
        super();
        this.onJoinChapter = this.onJoinChapter.bind(this);
        this.onMembersCount = this.onMembersCount.bind(this);
    }

    componentWillMount() {
        this.onMembersCount();
    }
    onMembersCount = () => {
        this.props.membersCount(this.props.chapter);
    }

    onJoinChapter = () => {
        this.props.joinChapter(this.props.chapter, this.props.user, getDate());
    }

    render() {
        const { chapter } = this.props;
        const { userLocation } = this.props;
        const members = this.props.members;
        const distance = geolib.getDistance({latitude: userLocation.latitude, longitude: userLocation.longitude},
            {latitude: chapter.chapterData.location.latitude, longitude: chapter.chapterData.location.longitude},10,3)
        return(
            <View style= {{flex:1, justifyContent: 'center'}}>
                <Header
                    containerStyle = {{ backgroundColor:'#378555',maxHeight:70, flex:1}}
                    leftComponent={{ icon: 'arrow-back', color: '#fff', underlayColor: 'transparent',onPress: () => Actions.pop() }}
                    leftContainerStyle = {{ marginBottom:15 }}
                    centerComponent={{ text: 'Join Chapter', style: { color: '#fff', fontSize:20, marginBottom:20 } }}
                />
                <AuthBackground/>
                <View>
                    <Card containerStyle = {styles.cardContainer}  title = {chapter.chapterData.title} titleStyle = {{fontSize:30}} dividerStyle= {{width:0}}>

                        <Divider style={{ backgroundColor: 'black', marginTop:15, marginBottom:15 }} />

                        <ListItem  title = {chapter.chapterData.additionalInfo}/>

                        <Divider style={{ backgroundColor: 'black', marginTop:15, marginBottom:15 }} />

                        <ListItem  title = "Members" 
                                    badge={{ value: members, textStyle: { color: 'black' }, containerStyle: { backgroundColor: 'transparent' } }}
                        />
                        <ListItem  title = "Distance"
                                    badge={{ value: (distance/1000).toFixed(2)+ " kms", textStyle: { color: 'black' }, containerStyle: { backgroundColor: 'transparent' } }}
                                    containerStyle = {{marginBottom:20}}
                        />

                        <Button title= "Join Chapter" 
                                buttonStyle = {styles.buttonStyle}
                                onPress = {this.onJoinChapter}        
                        />
                    </Card>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    cardContainer: {
        maxHeight: Dimensions.get('window').height-125
    },
    buttonStyle: {
        marginTop: Dimensions.get('window').height/8
    }
});

const mapDispatchToProps = {
    joinChapter,
    membersCount
}

const mapStateToProps = state => {
    return {
        userLocation: state.auth.userLocation,
        chapter: state.auth.selectedChapter,
        user: state.auth.user,
        members: state.auth.selectedChapterMemberCount
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChapterDetails)