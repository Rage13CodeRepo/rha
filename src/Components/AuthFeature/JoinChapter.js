import React, { Component } from "react";
import { StyleSheet, View, Dimensions, Text, BackHandler, DeviceEventEmitter, Platform } from "react-native";
import MapView, { Marker, Callout } from "react-native-maps";
import { connect } from "react-redux";
import { setUserLocation, getAllChapters, setChapter, requestLocationPermission } from "../../Actions/AuthAction";
import { FloatingAction } from "react-native-floating-action";
import { Icon, Header } from "react-native-elements";
import geolib from "geolib";

const { width, height } = Dimensions.get('window')
const _map = MapView;
const ASPECT_RATIO = width /height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO


class JoinChapter extends Component {

    constructor(props) {
        super(props);
        this.onSetUserLocation = this.onSetUserLocation.bind(this);
        this.getPosition = this.getPosition.bind(this);
        console.log(props.locationPermission);
    }
    
    onSetUserLocation = (location) => {
        this.props.setUserLocation(location);
    }

    goToChapterDetails = (chapterId, chapterData) => {
        const chapter = {chapterId: chapterId, chapterData: chapterData}
        this.props.setChapter(chapter);
    }

    getPosition = () => {
        navigator.geolocation.getCurrentPosition((position) => {
            let lat = parseFloat(position.coords.latitude);
            let lng = parseFloat(position.coords.longitude);
            let initialRegion = {
                latitude: lat,
                longitude: lng,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            }
            let location = {
                latitude: lat,
                longitude: lng
            }
            this.onSetUserLocation(location);
        },
        (error) => console.log(JSON.stringify(error)),
        {enableHighAccuracy: true, timeout: 5000, maximumAge: 1000 });
    }

    componentWillMount() {
        this.props.requestLocationPermission();
        this.getPosition();
        this.props.getAllChapters();
    }

    componentDidMount() {

    }

    componentWillReceiveProps(newProps) {
        if (this.props.locationPermission !== newProps.locationPermission)
            this.getPosition();
    }

    componentWillUnmount() {
        LocationServicesDialogBox.stopListener();
    }

    render() {
        
        const { chapters } = this.props;
        const { userLocation } = this.props;
        

        return(
            <View style={styles.container}>
            {
                !this.props.locationPermission ? <View></View> :
                <View style={styles.container}>
                <Header
                    containerStyle = {{ backgroundColor:'#378555',maxHeight:60, flex:1}}
                    centerComponent={{ text: 'Select Chapter Near You', style: { color: '#fff', fontSize:20, marginBottom:20 } }}
                />
                <MapView
                    style={styles.map}
                    showsUserLocation
                    showsMyLocationButton = {false}
                    showsCompass = {true}
                    loadingEnabled = {true}
                    minZoomLevel = {8}
                    showsCompass = {true}
                    region={{ 
                        latitude: parseFloat(userLocation.latitude),
                        longitude: parseFloat(userLocation.longitude),
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA
                    }}
                >
                {
                    chapters !== null &&
                    Object.keys(chapters).map(key=> {
                        const distance = geolib.getDistance({latitude: this.props.userLocation.latitude, longitude: this.props.userLocation.longitude},
                                        {latitude: chapters[key].location.latitude, longitude: chapters[key].location.longitude},10,3)
                        return(
                            <Marker
                                key = {key}
                                title = {chapters[key].title}
                                coordinate = {{latitude: parseFloat(chapters[key].location.latitude) , longitude: parseFloat(chapters[key].location.longitude)}}>
                                <Callout 
                                    onPress = {() => this.goToChapterDetails(key, chapters[key])}
                                >
                                    <Text style = {styles.textTitle}>{chapters[key].title}</Text>
                                    <Text style = {styles.textDistance}>{(distance/1000).toFixed(2)+ " kms" } </Text>
                                </Callout>
                            </Marker>
                        );
                    }) 
                }
                </MapView>
                <FloatingAction floatingIcon = {<Icon name='my-location' type='MaterialIcons' color='white' size = {35}/>}
                                overlayColor = "none"
                                style = {{backgroundColor:'black'}}
                                dismissKeyboardOnPress
                                onPressMain = {this.getPosition}
                                showBackground = {false}
                />
                </View>
                
            }
            </View>
        
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5FCFF"
    },
    map: {
        flex: 1,
    },
    textDistance: {
        textAlign: 'right',
        fontSize: 13
    },  
    textTitle: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 15,
        paddingBottom: 10,
    }  
});

const mapDispatchToProps = {
    setUserLocation,
    getAllChapters,
    setChapter,
    requestLocationPermission
}

const mapStateToProps = state => {
    return {
        userLocation: state.auth.userLocation,
        chapters: state.auth.chapters,
        loading: state.auth.loading,
        locationPermission:  state.auth.locationPermission
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(JoinChapter);