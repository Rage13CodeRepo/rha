import React, { Component } from "react";
import AuthBackground from "../Common/AuthBackground";
import { View } from "react-native";
import { Card, Input, Button, Text } from "react-native-elements";
import { StyleSheet } from "react-native";
import { connect } from "react-redux";
import { loginEmailChanged, loginPasswordChanged, loginUser, userAlreadyLoggedIn, clearError } from "../../Actions/AuthAction";
import { Actions } from "react-native-router-flux";
import Spinner from "../Common/Spinner";
import Toast, { DURATION } from 'react-native-easy-toast-fixed';

class Login extends Component {
    constructor(props) {
        super(props);
        this.onLoginUser = this.onLoginUser.bind(this);
    }
    
    componentWillMount() {
        this.props.clearError();
    }

    componentWillReceiveProps(newProps) {
        if(newProps.error != this.props.error) {
            if(newProps.error !== null)
                this.refs.toast.show(newProps.error, DURATION.FOREVER);
            else
                this.refs.toast.show();

        }      
    }

    onLoginUser = () => {
        const { email, password } = this.props;
        this.props.loginUser({email, password});
    }

    render() {
        
        return(
            <View style = {styles.loginContainer}>
                <AuthBackground/>
                { this.props.loading ? <Spinner/> :
                    <View>
                        <Card containerStyle = {styles.cardContainer}>
                            <Input
                                keyboardType = "email-address"
                                placeholder = "Enter your email here"
                                leftIcon = {{ type: 'MaterialIcons', name: 'email' }}
                                inputContainerStyle = {styles.inputContainerStyle}
                                containerStyle = {styles.containerStyle}
                                inputStyle = {{height:60}}
                                onChangeText = {(textInput) => this.props.loginEmailChanged(textInput)}
                                value = {this.props.email}
                            />
                            <Input
                                secureTextEntry
                                placeholder = "Password"
                                leftIcon = {{ type: 'MaterialIcons', name: 'lock-outline' }}
                                inputContainerStyle = {styles.inputContainerStyle}
                                containerStyle = {styles.containerStyle}
                                inputStyle = {{height:60}}
                                onChangeText = {(textInput) => this.props.loginPasswordChanged(textInput)}
                                value = {this.props.password}
                            />

                            <Button
                                title='Login'
                                buttonStyle = {styles.buttonStyle}
                                onPress = {this.onLoginUser}
                            />
                        
                        </Card>
                        <Toast
                            ref="toast"
                            style={{backgroundColor:'#D54F44', borderRadius:5}}
                            position='bottom'
                            positionValue={390}
                            opacity={0.8}
                            textStyle={{color:'white'}}
                        />
                        <Text style = {{textAlign:"center", color:'#ffffff', textDecorationLine: 'underline', textDecorationColor:'#ffffff', fontSize:15, fontWeight: 'bold' }} onPress = {() => Actions.signup({type:'replace'})}>Don't have an account ? Sign up !</Text>
                    </View>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    loginContainer:{
        flex:1,
        justifyContent: 'center',
    },
    inputContainerStyle: {
        marginTop: 15,
        height: 60,
        borderColor: '#252525',
        borderWidth: 1,
        borderRadius: 25,

    },
    containerStyle: {
        paddingBottom: 10,
        marginLeft: 15,
    },
    cardContainer: {
        marginTop:25,
        padding: 20,
        backgroundColor: 'rgba(239,247,253, 0.8)',
        marginBottom:100,
    },
    buttonStyle: {
        marginTop:15,
        marginBottom:15
    }
});

const mapDispatchToProps = {
    loginEmailChanged,
    loginPasswordChanged,
    loginUser,
    userAlreadyLoggedIn,
    clearError
}

const mapStateToProps = state => {
    return {
        email: state.auth.email_login,
        password: state.auth.password_login,
        loading: state.auth.loading,
        user: state.auth.user,
        error: state.auth.error
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);