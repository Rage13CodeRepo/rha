import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import MainHeader from "../Common/MainHeader";
import GroupChat from "./GroupChat";
import { connect } from "react-redux";

class Chat extends Component {
    render() {
        return(
            <View style = {styles.container}>
                <MainHeader title = "Chat" leftButton = "arrow-back" rightButton = "settings" leftAction = "back" rightAction = "chatsettings" />
    
                <GroupChat/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1
    }
});

const mapDispatchToProps = {

}

const mapStateToProps = state => {
    return{
        
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);