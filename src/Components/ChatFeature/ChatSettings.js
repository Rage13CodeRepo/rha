import React, { Component } from "react";
import { View, StyleSheet, Text, Switch } from "react-native";
import { ListItem } from "react-native-elements";
import { connect } from "react-redux";
import { changePushNotificationSettings, clearMessages} from "../../Actions/ChatActions";
import MainHeader from "../Common/MainHeader";

class ChatSettings extends Component {
    render() {
        return(
            <View style = {style.settingsContainer}>
                <MainHeader title = "Settings" leftButton = "arrow-back" rightButton = "none" leftAction = "back"/>
                
                <View style = {style.container}>
                    <View style = "">
                        <Text>General Chat</Text>
                    </View>
                    <ListItem 
                        switchButton
                        switched = {this.props.allowPushNotifications}
                        style = {{marginTop:20}}
                        containerStyle = {{backgroundColor: 'white'}}
                        titleStyle = {{}}
                        title = "Push Notifications"
                        switch = {<Switch disabled = {false} value = {false}></Switch>}
                        hideChevron
                        bottomDivider
                    />
                    <ListItem 
                        containerStyle = {{backgroundColor: 'white'}}
                        titleStyle = {{}}
                        title = "Clear Chat History"
                        hideChevron
                        onPress = {this.props.clearMessages}
                    />
                </View>
            </View>
        );
    }
}

const style = StyleSheet.create({
    settingsContainer: {
        flex:1,
    },
    container: {
        marginTop:5,
        padding: 20,
        backgroundColor: 'rgba(239,247,253, 0)',
        marginBottom:100,
    }
})

const mapDispatchToProps = {
    clearMessages,
    changePushNotificationSettings
}

const mapStateToProps = state => {
    return{
        allowPushNotifications: state.chat.allowPushNotifications
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(ChatSettings)