import React, { Component } from "react";
import { GiftedChat, Send } from "react-native-gifted-chat";
import { View } from "react-native";
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import { chatMessageChanged, messageSend, loadMessages } from "../../Actions/ChatActions";

class GroupChat extends Component {

  constructor(props) {
    super(props);
    this.renderSend = this.renderSend.bind(this);
    this.onLoadMessages = this.onLoadMessages.bind(this);
  }

  componentWillMount() {
    this.props.loadMessages(this.props.usersAdditionalInfo);
  }

  onLoadMessages() {
    
  }

  renderSend = (props) => {
    return(
      <Send
        {...props}
      >
        <View style = {{marginBottom: 10, marginRight: 10}}>
          <Icon
            name = "send"
            type = "MaterialIcon"
            color = "#378555"
          />
        </View>
      </Send>
    );
  }

  render() {
    return (
      <GiftedChat
        messages={this.props.messages}
        onInputTextChanged = {text => this.props.chatMessageChanged(text)}
        onSend={textMessage => this.props.messageSend(textMessage)}
        user={{
          _id: this.props.user.user.uid,
          name: this.props.usersAdditionalInfo.name,
          avatar: 'https://placeimg.com/140/140/any',
          chapter: this.props.usersAdditionalInfo.affiliatedChapter
        }}
        renderSend = {this.renderSend}
      />
    )
  }
}

const mapDispatchToProps = {
  chatMessageChanged,
  messageSend,
  loadMessages
}

const mapStateToProps = state => {
  return {
    message: state.chat.message,
    messages: state.chat.messages,
    user: state.auth.user,
    usersAdditionalInfo: state.auth.usersAdditionalInfo
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupChat);