import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import MainHeader from "../Common/MainHeader";

class News extends Component {
    render() {
        return(
            <View style = {styles.container}>
                <MainHeader title = "Home" leftButton = "menu" rightButton = "settings" />
                <Text>This is the Home Component</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1
    }
});

export default News;