import React from "react";
import { Router, Scene, Overlay, Actions, Modal, Tabs } from "react-native-router-flux";
import Login from "./Components/AuthFeature/Login";
import Signup from "./Components/AuthFeature/Signup";
import AdditionalInfo from "./Components/AuthFeature/AdditionalInfo";
import JoinChapter from "./Components/AuthFeature/JoinChapter";
import ChapterDetails from "./Components/AuthFeature/ChapterDetails"
import Spinner from "./Components/Common/Spinner"
import Splash from "./Components/Common/Splash"
import News from "./Components/NewsFeature/Index";
import Events from "./Components/EventsFeature/Index";
import Chat from "./Components/ChatFeature/Index";
import ChatSettings from "./Components/ChatFeature/ChatSettings";
import { Icon } from "react-native-elements";
import Upcoming from "./Components/EventsFeature/Upcoming";
import Completed from "./Components/EventsFeature/Completed";
import Ongoing from "./Components/EventsFeature/Ongoing";
import AddDrive from "./Components/EventsFeature/AddDrive";
import MainHeader from "./Components/Common/MainHeader";
import GeneralSetting from "./Components/SettingFeature/GeneralSettings";
import CardStackStyleInterpolator from 'react-navigation-stack/dist/views/StackView/StackViewStyleInterpolator';

const RouterComponent = () => {
    return(
        <Router  >
            <Overlay key = "overlay" >
            <Modal hideNavBar>
                <Scene key="modal" modal hideNavBar>
                <Scene key = "root" navigationBarStyle = {{backgroundColor:'#378555', color:'white'}} sceneStyle = {{backgroundColor:'transparent'}} 
                >
                    <Scene key = "auth" hideNavBar >
                        <Scene key = "splash" component = { Splash } hideNavBar />
                        <Scene key = "login" component = { Login } hideNavBar />
                        <Scene key = "signup" component = { Signup } hideNavBar />
                        <Scene key = "additionalinfo" component = { AdditionalInfo } hideNavBar />
                        <Scene key = "joinchapter" component = { JoinChapter } hideNavBar  />
                        <Scene key = "chapterdetails" component = { ChapterDetails }   title = "Join Chapter" titleStyle = {{color:'white'}}  direction="right" hideNavBar />
                        <Scene key = "spinner" component = { Spinner } hideNavBar />
                    </Scene>
                    <Scene key = "main" hideNavBar 
                    transitionConfig={() => ({
                   
                        screenInterpolator: (props) => {
                            switch (props.scene.route.params.direction) {
                                case 'vertical':
                                    return CardStackStyleInterpolator.forFadeFromBottomAndroid(props);
                                case 'fade':
                                    return CardStackStyleInterpolator.forFade(props);
                                case 'none':
                                    return CardStackStyleInterpolator.forInitial
                                case 'horizontal':
                                    return CardStackStyleInterpolator.forHorizontal(props);
                                default:
                                    return CardStackStyleInterpolator.forHorizontal(props);
                            }
                        }
                    })}>
                        <Scene key = "tabbarnavigation" tabs  tabBarStyle = {{height:60}} showLabel = {false} hideNavBar initial activeTintColor = {"#89C440"} labelStyle = {{color:'#494f42' }} >
                            <Scene key = "news" component = { News } type="reset" labelStyle ={{fontSize:0}} hideNavBar icon={({ focused }) => (
                                <Icon
                                    type = "MaterialIcons"
                                    name={focused ? 'home' : 'home'}
                                    size={30}
                                    color={focused ? '#89C440' : '#494f42'}
                                />)}
                            />
                            <Scene key = "events" labelStyle ={{fontSize:0}} icon={({ focused }) => (
                                <Icon
                                    type = "MaterialIcons"
                                    name={focused ? 'event' : 'event'}
                                    size={30}
                                    color={focused ? '#89C440' : '#494f42'}
                                />)}
                                panHandlers={null}
                                title = "Drives"
                                navBar = { MainHeader }
                            >
                                <Scene key = "eventstabbarnavigation" tabBarStyle = {{height:50, backgroundColor: '#fff' }} tabStyle = {{ activeTintColor: '#89C440' }} lazy labelStyle = {{color:'#494f42' }} showLabel = {true} tabs tabBarPosition = "top" activeTintColor = "#378555" indicatorStyle = {{color:'#89C440'}} >
                                        <Scene key="upcoming" component = {Upcoming} title = "Upcoming" tintColor = '#89C440' hideNavBar />
                                        <Scene key="ongoing" component = {Ongoing} title = "Ongoing" tintColor = '#89C440' hideNavBar />
                                        <Scene key="completed" component = {Completed} title = "Completed" tintColor = '#89C440' hideNavBar />
                                </Scene>
                             
                            </Scene>
                            <Scene key = "chat" component = {Spinner}  tabBarOnPress = {() => {return Actions.chatmodal();}} labelStyle ={{fontSize:0}}  hideNavBar icon={({ focused }) => (
                                <Icon
                                    type = "MaterialIcons"
                                    name={focused ? 'chat' : 'chat'}
                                    size={30}
                                    color={focused ? '#89C440' : '#494f42'}
                                />)}
                            />
                           
                        </Scene>
                        <Scene key = "chatmodal" component = { Chat } direction = "vertical" hideNavBar/>
                        <Scene key = "chatsettings" component = { ChatSettings } direction = "horizontal" hideNavBar/>
                        <Scene key = "adddrive" component = { AddDrive } direction = "vertical" hideNavBar />
                        <Scene key = "generalsettings" component = { GeneralSetting } direction = "horizontal" hideNavBar initial  />
                        

                    </Scene>
                    </Scene>
                </Scene>
            </Modal>
            </Overlay>
        </Router>
    );
}

export default RouterComponent;