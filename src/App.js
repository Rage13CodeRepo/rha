import React, { Component }from "react";
import { StyleSheet, View, Image, StatusBar, AsyncStorage } from "react-native";
import Router from "./Router";
import reducers from "./Reducers";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore, persistReducer } from "redux-persist";
import { PersistGate } from 'redux-persist/integration/react'

export default class App extends Component {
    constructor() {
        super();
        console.ignoredYellowBox = ['Setting a timer'];
        
        state = {
            isReady: false
        }
    }


    render() {
        const persistConfig = {
            key: 'rha-v-1.0.1',
            storage: AsyncStorage
        }

        const persistedReducer = persistReducer(persistConfig, reducers)

        //const composeEnhancers = composeWithDevTools({realtime: true, port: 8080});
        const store = createStore(persistedReducer, composeWithDevTools(applyMiddleware(ReduxThunk)));

        const persiststor = persistStore(store);
        
        return(
            <Provider store = {store} >
                <PersistGate persistor={persiststor} loading = {null}>
                    <View style = {styles.appContainer}>
                        <StatusBar backgroundColor="#378555" barStyle="light-content"/>
                        <Router />
                    </View>
                </PersistGate>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    appContainer: {
        flex:1,
        justifyContent: 'center',
      
    }
});